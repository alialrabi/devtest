(function(){

  var app = angular.module('notesApp',['ngRoute', 'ngMaterial']);

  app.config(['$locationProvider', '$routeProvider',
      function ($locationProvider, $routeProvider) {

        $routeProvider
          .when('/', {
            templateUrl: '/partials/notes-view.html',
            controller: 'notesController'
          })
          .when('/login', {
             templateUrl: '/partials/login.html',
             controller: 'loginController',
          })
          .otherwise('/');
      }
  ]);   


  app.run(['$rootScope', '$location', 'AuthService', 'NoteService', function ($rootScope, $location, AuthService) {
      $rootScope.$on('$routeChangeStart', function (event) {

          if ($location.path() == "/login"){
             return;
          }

          if (!AuthService.isLoggedIn()) {
              console.log('DENY');
              $location.path('/login');
          }
          
      });
  }]);

    app.service('AuthService', function($http){
        var loggedUser = null;

        function login (username, password){
            return $http.post("api/login", {username: username, password: password});
        }

        function setLoggedUser(user){
            loggedUser = user;
            if(loggedUser !== null) {
                localStorage.setItem("loggedUser", JSON.stringify(loggedUser));
            }else{
                localStorage.removeItem("loggedUser");
            }
        }

        function isLoggedIn(){
            if(loggedUser != null){
                return true;
            }else{
                let user = localStorage.getItem("loggedUser");
                if(user !== null && user !== undefined && user !== ''){
                    loggedUser = JSON.parse(user);
                    return true;
                }else{
                    return false;
                }
            }
        }
        return {
            login : login,
            isLoggedIn: isLoggedIn,
            setLoggedUser: setLoggedUser
        }
        
  });
  


  app.controller('loginController', function($scope, AuthService, $location){

    $scope.invalidCreds = false;
    $scope.login = {
        username : null,
        password : null
    };

    $scope.login = function(){
        $scope.invalidCreds = false;
        AuthService.login($scope.login.username, $scope.login.password).then(function(user){
            AuthService.setLoggedUser(user);
            $location.path("/");
        }, function(error){
            AuthService.setLoggedUser(null);
            $scope.invalidCreds = true;
        });
    };
    
  });

  app.service('NoteService', function($http){

        function saveNote (data){
            return $http({
                  method : 'POST',
                  url : 'save',
                  data : data
            });
        }
        
        function getAll (){
            return $http({
                  method : 'GET',
                  url : 'getall',
                  headers:'Accept:application/json'
            }).then(function mySuccess(response) {
                return response.data;
            }, function myError(response) {
               return "NO Notes";
            });
        }
        
        return {
           saveNote:saveNote,
           getAll: getAll
        }
  });
  
  app.controller('notesController', function($scope, NoteService, $location){
  
    $scope.isEditCreateView = false;
    
    $scope.note = {
        name : null,
        noteText : null
    };
    
    $scope.getNotes = function() {
		    NoteService.getAll().then(function(data) {
		      $scope.notes  = data;
	       });
	}
    
    $scope.getNotes();
    
    $scope.newNoteView = function(){
        $scope.isEditCreateView = true;
    };
    
     $scope.newNote = function(){
        $scope.isEditCreateView = true;
        NoteService.saveNote($scope.note).then(function(note){
            $scope.getNotes();
            $location.path("/");
        }, function(error){
            console.log(error);
        });
    };
    
     $scope.editNoteView = function(name){
        $scope.isEditCreateView = true;
        $scope.note = $scope.notes.filter(function(item) {
          return item.name === name;
        })[0];
    };

    $scope.deleteNote = function (i) {
      var r = confirm("Are you sure you want to delete this note?");
      if (r == true){
        //TODO delete the note
      }
    };
    
    $scope.viewNote = function(){
        //TODO
    }
   
    $scope.cancel = function(){
        $scope.isEditCreateView = false;
        $scope.note = {
        name : null,
        noteText : null
    };
    }
      });

})();
package com.thevirtugroup.postitnote.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;

/**
 * 
 * @author ali
 *
 */
@RestController
public class NoteController{
	
	Logger log=Logger.getLogger(NoteController.class);
	
	@Autowired
	NoteRepository noteRepository;

	public NoteController() {
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
    public void save(@RequestBody Note note){
		log.info(note);
		noteRepository.save(note);
    }
	
	@RequestMapping(value = "getall", method = RequestMethod.GET)
	@ResponseBody
    public List<Note> getall(){
		Map<String,Note> notesMap= noteRepository.getAll();
		List<Note> notes=new ArrayList<Note>(notesMap.values());
		log.debug(notes);
        return notes;
    }
	
}

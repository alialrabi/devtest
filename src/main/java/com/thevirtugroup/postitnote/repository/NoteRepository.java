package com.thevirtugroup.postitnote.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.thevirtugroup.postitnote.model.Note;

/**
 * 
 * @author ali
 *
 */
@Repository
public class NoteRepository {
	
	Map<String,Note> notes=new HashMap<String, Note>();
			
	public void save(Note note) {
		Optional<Note> obj = notes
			    .values()
			    .stream()
			    .filter(e ->  e.getName().equals(note.getName()))
			    .findFirst();
		
		if(obj.isPresent()) {
		  Note updateObj=obj.get();	
		  updateObj.setName(note.getName());
		  updateObj.setNoteText(note.getNoteText());
		  notes.put(note.getName(),updateObj);
		}else {
			Note newNote=new Note();
			newNote.setName(note.getName());
			newNote.setNoteText(note.getNoteText());
			notes.put(note.getName(),newNote);
		}
	}
	
	public Map<String,Note> getAll() {
		return notes;
	}
	
}
